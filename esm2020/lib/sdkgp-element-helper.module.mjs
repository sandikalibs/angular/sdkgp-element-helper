import { NgModule } from '@angular/core';
import { SdkgpElementHelperComponent } from './sdkgp-element-helper.component';
import * as i0 from "@angular/core";
export class SdkgpElementHelperModule {
}
SdkgpElementHelperModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: SdkgpElementHelperModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
SdkgpElementHelperModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: SdkgpElementHelperModule, declarations: [SdkgpElementHelperComponent], exports: [SdkgpElementHelperComponent] });
SdkgpElementHelperModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: SdkgpElementHelperModule });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: SdkgpElementHelperModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [
                        SdkgpElementHelperComponent
                    ],
                    imports: [],
                    exports: [
                        SdkgpElementHelperComponent
                    ]
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2RrZ3AtZWxlbWVudC1oZWxwZXIubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vcHJvamVjdHMvc2RrZ3AtZWxlbWVudC1oZWxwZXIvc3JjL2xpYi9zZGtncC1lbGVtZW50LWhlbHBlci5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsMkJBQTJCLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQzs7QUFjL0UsTUFBTSxPQUFPLHdCQUF3Qjs7cUhBQXhCLHdCQUF3QjtzSEFBeEIsd0JBQXdCLGlCQVJqQywyQkFBMkIsYUFLM0IsMkJBQTJCO3NIQUdsQix3QkFBd0I7MkZBQXhCLHdCQUF3QjtrQkFWcEMsUUFBUTttQkFBQztvQkFDUixZQUFZLEVBQUU7d0JBQ1osMkJBQTJCO3FCQUM1QjtvQkFDRCxPQUFPLEVBQUUsRUFDUjtvQkFDRCxPQUFPLEVBQUU7d0JBQ1AsMkJBQTJCO3FCQUM1QjtpQkFDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBTZGtncEVsZW1lbnRIZWxwZXJDb21wb25lbnQgfSBmcm9tICcuL3Nka2dwLWVsZW1lbnQtaGVscGVyLmNvbXBvbmVudCc7XG5cblxuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtcbiAgICBTZGtncEVsZW1lbnRIZWxwZXJDb21wb25lbnRcbiAgXSxcbiAgaW1wb3J0czogW1xuICBdLFxuICBleHBvcnRzOiBbXG4gICAgU2RrZ3BFbGVtZW50SGVscGVyQ29tcG9uZW50XG4gIF1cbn0pXG5leHBvcnQgY2xhc3MgU2RrZ3BFbGVtZW50SGVscGVyTW9kdWxlIHsgfVxuIl19