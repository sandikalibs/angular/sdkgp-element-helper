import * as i0 from "@angular/core";
import * as i1 from "./sdkgp-element-helper.component";
export declare class SdkgpElementHelperModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<SdkgpElementHelperModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<SdkgpElementHelperModule, [typeof i1.SdkgpElementHelperComponent], never, [typeof i1.SdkgpElementHelperComponent]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<SdkgpElementHelperModule>;
}
