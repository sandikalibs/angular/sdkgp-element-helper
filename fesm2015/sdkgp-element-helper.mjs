import * as i0 from '@angular/core';
import { Injectable, Component, NgModule } from '@angular/core';

class SdkgpElementHelperService {
    constructor() { }
}
SdkgpElementHelperService.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: SdkgpElementHelperService, deps: [], target: i0.ɵɵFactoryTarget.Injectable });
SdkgpElementHelperService.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: SdkgpElementHelperService, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: SdkgpElementHelperService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return []; } });

class SdkgpElementHelperComponent {
}
SdkgpElementHelperComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: SdkgpElementHelperComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
SdkgpElementHelperComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "15.2.9", type: SdkgpElementHelperComponent, selector: "lib-sdkgp-element-helper", ngImport: i0, template: `
    <p>
      sdkgp-element-helper works!
    </p>
  `, isInline: true });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: SdkgpElementHelperComponent, decorators: [{
            type: Component,
            args: [{ selector: 'lib-sdkgp-element-helper', template: `
    <p>
      sdkgp-element-helper works!
    </p>
  ` }]
        }] });

class SdkgpElementHelperModule {
}
SdkgpElementHelperModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: SdkgpElementHelperModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
SdkgpElementHelperModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: SdkgpElementHelperModule, declarations: [SdkgpElementHelperComponent], exports: [SdkgpElementHelperComponent] });
SdkgpElementHelperModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: SdkgpElementHelperModule });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: SdkgpElementHelperModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [
                        SdkgpElementHelperComponent
                    ],
                    imports: [],
                    exports: [
                        SdkgpElementHelperComponent
                    ]
                }]
        }] });

/*
 * Public API Surface of sdkgp-element-helper
 */

/**
 * Generated bundle index. Do not edit.
 */

export { SdkgpElementHelperComponent, SdkgpElementHelperModule, SdkgpElementHelperService };
//# sourceMappingURL=sdkgp-element-helper.mjs.map
